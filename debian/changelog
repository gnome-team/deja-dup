deja-dup (45.2-2) unstable; urgency=medium

  * Update debian/watch
  * Update Homepage
  * Bump Standards Version to 4.7.0

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 16 Dec 2024 15:34:38 -0500

deja-dup (45.2-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bícha <jbicha@ubuntu.com>  Tue, 02 Jan 2024 10:48:52 -0500

deja-dup (45.1-1) unstable; urgency=medium

  * New upstream release
  * Bump minimum GTK4 to 4.12 and libadwaita to 1.4
  * Stop using debian/control.in and dh_gnome_clean

 -- Jeremy Bícha <jbicha@ubuntu.com>  Sat, 25 Nov 2023 17:45:22 -0500

deja-dup (44.2-2) unstable; urgency=medium

  * Drop obsolete Recommends: policykit-1

 -- Jeremy Bícha <jbicha@ubuntu.com>  Wed, 21 Jun 2023 20:02:08 -0400

deja-dup (44.2-1) unstable; urgency=medium

  * New upstream release

 -- Sebastien Bacher <seb128@ubuntu.com>  Fri, 16 Jun 2023 16:07:03 +0200

deja-dup (44.1-1) unstable; urgency=medium

  * Team upload

  [ Jarred Wilson ]
  * New upstream release

  [ Jeremy Bicha ]
  * Add debian/upstream/metadata
  * Remove an obsolete maintscript entry
  * Update standards version to 4.6.2, no changes needed

 -- Jarred Wilson <jarred.wilson@canonical.com>  Tue, 16 May 2023 11:59:02 -0400

deja-dup (44.0-2) unstable; urgency=medium

  * debian/control:
    - Suggests: python3-pydrive2
  * debian/rules:
    - set -Dpydrive_pkgs=python3-pydrive2 since it's not available in Debian
  * debian/patches/disable-google-drive.patch:
    - removed, it's not needed anymore

 -- Sebastien Bacher <seb128@ubuntu.com>  Mon, 16 Jan 2023 16:51:50 +0100

deja-dup (44.0-1) unstable; urgency=medium

  * New upstream release
  * debian/control.in:
    - Updated the libadwaita and packaging requirements
  * debian/rules:
    - explicitly set -Dpackagekit and -Drequests_oauthlib_pkgs

 -- Sebastien Bacher <seb128@ubuntu.com>  Fri, 02 Dec 2022 11:48:20 +0100

deja-dup (43.4-1) unstable; urgency=medium

  * New upstream release
  * Bump debhelper-compat to 13
  * debian/control.in: Bump minimum libadwaita to 1.1.0
  * debian/control.in: Set Rules-Requires-Root: no
  * debian/rules: Drop no longer needed -Wl,--as-needed
  * debian/rules: Set -Wl,-z,defs

 -- Jeremy Bicha <jbicha@ubuntu.com>  Wed, 24 Aug 2022 12:40:04 -0400

deja-dup (43.3-1) unstable; urgency=medium

  * New upstream release
  * debian/control.in:
    - updated the requirements
  * debian/patches/which_removal.patch:
    - remove, the issue is fixed in the new version

 -- Sebastien Bacher <seb128@debian.org>  Fri, 20 May 2022 15:32:03 +0200

deja-dup (42.9-1) unstable; urgency=medium

  * New upstream release

 -- Sebastien Bacher <seb128@ubuntu.com>  Mon, 07 Feb 2022 13:39:58 +0100

deja-dup (42.8-2) unstable; urgency=medium

  * d/p/which_removal.patch: Fix a test that is using the deprecated "which"
    command.  Use "command -v" as recommended
  * debian/control.in: Bump Standards-Version to 4.6.0 (no further changes)

 -- Laurent Bigonville <bigon@debian.org>  Sat, 25 Sep 2021 15:14:57 +0200

deja-dup (42.8-1) unstable; urgency=medium

  * New upstream release

 -- Sebastien Bacher <seb128@ubuntu.com>  Mon, 23 Aug 2021 11:02:32 +0200

deja-dup (42.7-1) unstable; urgency=medium

  * New upstream release
  * debian/control.in:
    - build with the new libhandy version

 -- Sebastien Bacher <seb128@ubuntu.com>  Fri, 19 Feb 2021 17:38:53 +0100

deja-dup (42.6-1) unstable; urgency=medium

  * New upstream release

 -- Sebastien Bacher <seb128@ubuntu.com>  Tue, 01 Dec 2020 08:13:27 +0100

deja-dup (42.5-1) unstable; urgency=medium

  * New upstream release

 -- Sebastien Bacher <seb128@ubuntu.com>  Wed, 28 Oct 2020 21:27:06 +0100

deja-dup (42.2-1) unstable; urgency=medium

  * New upstream release, mount partition if needed (lp: #1761216)
  * debian/patches/translation_tests_fix.patch:
    - removed, included in the new version

 -- Sebastien Bacher <seb128@ubuntu.com>  Wed, 19 Aug 2020 15:51:02 +0200

deja-dup (42.0-1) unstable; urgency=medium

  * New upstream release
  * debian/copyright:
    - remove entry for the nautilus sources which have been removed
  * debian/control.in:
    - use libhandy now
    - don't use libgoa nor nautilus, deprecated in the new version
    - updated the glib requirement
  * debian/control.in, debian/rules:
    - remove boto and switch suggests and build option, those backends
      aren't shipped anymore
  * debian/patches/translation_tests_fix.patch:
    - update the translation template dir for the package build directories

 -- Sebastien Bacher <seb128@ubuntu.com>  Mon, 29 Jun 2020 18:25:31 +0200

deja-dup (40.7-1) unstable; urgency=medium

  * New stable update

 -- Sebastien Bacher <seb128@ubuntu.com>  Mon, 15 Jun 2020 15:18:57 +0200

deja-dup (40.6-1) unstable; urgency=medium

  * New upstream release

 -- Sebastien Bacher <seb128@ubuntu.com>  Tue, 24 Mar 2020 21:19:59 +0100

deja-dup (40.5-1) unstable; urgency=medium

  * New upstream release
  * Remove patch included in the new version
  * debian/patches/disable-google-drive.patch:
    - disable the Google Drive backend and default to local for backups,
      the option requires pydrive which isn't available in Debian yet.
      Thanks Mike Terry for the patch. (Closes: #944510)

 -- Sebastien Bacher <seb128@ubuntu.com>  Fri, 29 Nov 2019 15:47:52 +0100

deja-dup (40.1-4) unstable; urgency=medium

  * Depend on duplicity on Ubuntu too (now that duplicity adopted Python3)
  * Update configure flags for Python3 package names
  * Bump minimum meson to 0.47 and libglib2.0-dev to 2.56
  * Build-Depend on itstool instead of yelp-tools
  * Build-Depend on dh-sequence-gnome instead of gnome-pkg-tools
  * Build-Depend on debhelper-compat 12 and drop debian/compat
  * Stop overriding libexecdir
  * Drop obsolete dh_strip dbgsym migration rule
  * Bump Standards-Version to 4.4.1

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 27 Oct 2019 10:00:09 -0400

deja-dup (40.1-3) unstable; urgency=medium

  * Team upload.
  * Cherry-pick patch from upstream to fix GPG_ERR redefinition

 -- Andreas Henriksson <andreas@fatal.se>  Mon, 30 Sep 2019 09:54:01 +0200

deja-dup (40.1-2) unstable; urgency=medium

  * debian/control.in: Switch the suggested python modules to their python3
    counter part or remove them if they are not available

 -- Laurent Bigonville <bigon@debian.org>  Sun, 29 Sep 2019 16:23:06 +0200

deja-dup (40.1-1) unstable; urgency=medium

  * New upstream release
  * debian/control.in: Bump Standards-Version to 4.4.0 (no further changes)
  * Drop debian/patches: all patches merged upstream
  * debian/control.in: Adjust the build-dependencies
  * Update the debian/copyright based on the upstream one

 -- Laurent Bigonville <bigon@debian.org>  Wed, 10 Jul 2019 13:04:07 +0200

deja-dup (38.4-1) experimental; urgency=medium

  * New upstream release (LP: #1823421)
    - Be more forgiving if packagekit is unresponsive
    - Don't run monitor when automatic backups are disabled
    - Drop libpeas dependency
    - Fix compilation with valac 0.43
    - Update app icon
    - Update translations
  * Drop libpeas BD per upstream
  * debian/docs: NEWS is now called NEWS.md
  * Take patch from upstream MR !22 to fix many tests under GLib 2.59
    g_key_file_get_comment() used to append a stray newline to its return
    value, and it stopped doing this with 2.59. deja-dup was accidentally
    relying on that behaviour.
  * Take patch from upstream MR !22 to fix whitespace assumption in test.
    Since GLib 2.59.1, g_format_size() started using non-breaking spaces
    instead of regular ones to separate the size from the unit. deja-dup was
    only testing for the latter. Update the test to use a regex with `\s`, so
    we match all types of whitespace.

 -- Iain Lane <laney@debian.org>  Wed, 10 Apr 2019 13:56:48 +0100

deja-dup (38.3-1) unstable; urgency=medium

  * New upstream release:
    - If some files couldn't be backed up, don't override that message
      with successful verification message. (lp: #1371613)
    - Tell user how much free space is needed when backup location is tiny
      (lp: #1103567)

 -- Sebastien Bacher <seb128@ubuntu.com>  Thu, 17 Jan 2019 11:24:31 +0100

deja-dup (38.2-1) unstable; urgency=medium

  * New upstream release:
    - Fix not being able to find the backup files when
      restoring on a fresh install (lp: #1804744)

 -- Sebastien Bacher <seb128@ubuntu.com>  Fri, 11 Jan 2019 16:23:43 +0100

deja-dup (38.1-1) unstable; urgency=medium

  * New upstream release
  * Bump Standards-Version to 4.2.1

 -- Jeremy Bicha <jbicha@debian.org>  Fri, 14 Dec 2018 20:42:18 -0500

deja-dup (38.0-1) unstable; urgency=medium

  * New upstream release
  * Drop all patches: Applied in new release
  * Bump Standards-Version to 4.1.4
  * Release to unstable

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 08 May 2018 18:39:38 -0400

deja-dup (37.1-2) experimental; urgency=medium

  * Add 0002-don-t-use-ulimit.patch:
    - Stop using ulimit since it is incompatible with webkit2gtk 2.20
      (LP: #1751460)

 -- Jeremy Bicha <jbicha@debian.org>  Fri, 16 Mar 2018 14:31:28 -0400

deja-dup (37.1-1) experimental; urgency=medium

  * New upstream unstable release
  * debian/watch: watch for unstable releases
  * Cherry pick upstream patch to exclude snap cache directories
  * maintscript: Bump version since Ubuntu didn't take this until now

 -- Jeremy Bicha <jbicha@debian.org>  Fri, 26 Jan 2018 20:23:14 -0500

deja-dup (36.3-2) unstable; urgency=medium

  * debian/copyright: Add debian/ to Files-Excluded
  * debian/watch: Only watch for stable releases
  * Update Vcs fields for migration to https://salsa.debian.org/
  * Bump debhelper compat to 11
  * Add debian/docs to install NEWS
  * Don't skip dh_auto_test
  * Don't depend on duplicity on Ubuntu (to not install python2 by default).
    Deja Dup will install duplicity if needed using PackageKit.

 -- Jeremy Bicha <jbicha@debian.org>  Fri, 26 Jan 2018 20:03:11 -0500

deja-dup (36.3-1) unstable; urgency=medium

  * New upstream version 36.3

 -- Laurent Bigonville <bigon@debian.org>  Mon, 13 Nov 2017 08:57:49 +0100

deja-dup (36.2-1) unstable; urgency=medium

  * New upstream version 36.2
  * debian/control.in: Bump Standards-Version to 4.1.1 (no further changes)

 -- Laurent Bigonville <bigon@debian.org>  Thu, 12 Oct 2017 00:55:18 +0200

deja-dup (36.1-1) unstable; urgency=medium

  * New upstream version 36.1
  * Drop debian/patches/01_xdg_autostart_exec.patch, applied upstream

 -- Laurent Bigonville <bigon@debian.org>  Sun, 17 Sep 2017 12:09:06 +0200

deja-dup (35.6-1) unstable; urgency=medium

  * debian/gbp.conf: Filter upstream debian/ directory
  * New upstream version 35.6
  * debian/maintscript: Rename the xdg autostart file to
    org.gnome.DejaDup.Monitor.desktop
  * debian/patches/01_xdg_autostart_exec.patch: Use sh instead of bash so we
    don't need to depend on it

 -- Laurent Bigonville <bigon@debian.org>  Thu, 07 Sep 2017 09:54:16 +0200

deja-dup (35.5-1) experimental; urgency=medium

  * New upstream release.
    - Adjust the build-dependencies
  * debian/rules:
    - Really use meson/ninja for the build, upstream is shipping a Makefile
      this makes debhelper use that file instead of ninja.
    - Define the name of the package that can be installed via packagekit
  * debian/control.in:
    - Update the description to reflect that deja-dup now supports owncloud
    - Downgrade python-cloudfiles and python-boto to Suggests, those backends
      are now hidden by default, but still supported for old users

 -- Laurent Bigonville <bigon@debian.org>  Sun, 27 Aug 2017 14:05:41 +0200

deja-dup (35.3-1) experimental; urgency=medium

  * New upstream release.
    - Switch to meson build system
    - Bump the build-dependencies
    - Drop debian/patches/help2man.patch, not needed anymore
  * debian/control.in: Bump Standards-Version to 4.0.0 (no further changes)
  * Update the debian/copyright file, based on ubuntu one

 -- Laurent Bigonville <bigon@debian.org>  Fri, 11 Aug 2017 16:55:44 +0200

deja-dup (34.3-1) unstable; urgency=medium

  * New upstream release.
  * Update Vcs-Browser to use cgit instead of gitweb.
  * Drop --parallel from debhelper options. It's the default with compat 10.

 -- Michael Biebl <biebl@debian.org>  Thu, 08 Dec 2016 23:01:58 +0100

deja-dup (34.2-2) unstable; urgency=medium

  * Bump debhelper compat level to 10.
  * Drop override for dh_auto_install, not needed.
  * Bump Build-Depends on libnautilus-extension-dev to (>= 3.21.92-3~).
    This ensures we have a version with multiarch support.

 -- Michael Biebl <biebl@debian.org>  Fri, 30 Sep 2016 16:42:47 +0200

deja-dup (34.2-1) unstable; urgency=medium

  * New upstraem release.
    - Drop debian/patches/01_ignore_appstream.patch, applied upstream
  * debian/watch: Check the gpg signature for the downloaded tarball
  * debian/control.in: Bump Standards-Version to 3.9.8 (no furher changes)
  * Drop -dbg package and rely on automatically built -dbgsym one
  * debian/rules: Drop ENABLE_CCPANEL variable, support for building the GNOME
    Control Center plugin has been removed.
  * debian/control.in: Update Vcs-* URL's to please lintian (again)
  * Update the debian/copyright file

 -- Laurent Bigonville <bigon@debian.org>  Mon, 25 Apr 2016 15:23:46 +0200

deja-dup (34.1-1) unstable; urgency=medium

  * New upstraem release.
    - Refresh debian/patches/01_ignore_appstream.patch
    - Add libpackagekit-glib2-dev to the build-dependencies
  * debian/patches/01_ignore_appstream.patch: Add appstream key to ignore
    some .desktop files
  * debian/patches/help2man.patch: Use system help2man instead of embedded
    copy to allow reproducible build (Closes: #805787)
  * Suggest python-swiftclient for OpenStack Swift support

 -- Laurent Bigonville <bigon@debian.org>  Fri, 25 Dec 2015 00:10:56 +0100

deja-dup (34.0-1) unstable; urgency=medium

  * Team upload.
  * New upstraem release.
  * Update outdated section names in debian/gbp.conf.

 -- Michael Biebl <biebl@debian.org>  Tue, 16 Jun 2015 16:54:00 +0200

deja-dup (32.0-1) unstable; urgency=medium

  * New upstream release
  * debian/control.in: Bump Standards-Version to 3.9.6 (no further changes)
  * debian/copyright: Fix copyright file, files have been moved around

 -- Laurent Bigonville <bigon@debian.org>  Sun, 12 Oct 2014 10:51:13 +0200

deja-dup (30.0-1) unstable; urgency=medium

  * New upstream release
    - Drop debian/patches/01_CMP0004_OLD.patch: Fixed upstream
  * Add debian/gbp.conf file
  * debian/control.in: Re-add the Vcs-* fields with the new URL's

 -- Laurent Bigonville <bigon@debian.org>  Fri, 11 Apr 2014 22:21:25 +0200

deja-dup (29.5-1) unstable; urgency=low

  * New upstream release (Closes: #667722, #671479)
    - Adjust the {build-}dependencies
    - debian/rules, debian/control: deja-dup is now using cmake
  * Adopt deja-dup by the GNOME team (Closes: #740924)
  * debian/control: Bump Standards-Version to 3.9.5 (no further changes)
  * Add debian/patches/01_CMP0004_OLD.patch: Fix FTBFS due to changes in cmake
  * debian/rules: Drop the override_dh_builddeb target, this is not needed
    anymore
  * Fix the debian/watch URL
  * Bump debhelper compatibility to 9 to get the hardening flags for free
  * debian/rules: Properly exclude nautilus plugin from dh_makeshlibs call
    (Closes: #651279)
  * debian/control: Adjust the recommends, add gvfs-backends and rename
    python-rackspace-cloudfiles to python-cloudfiles (Closes: #669045, #732730)

 -- Laurent Bigonville <bigon@debian.org>  Thu, 06 Mar 2014 22:11:37 +0100

deja-dup (20.2-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "configure:3028: error: xmllint not found":
    change build dependency from itstool to yelp-tools; thanks, Michael Biebl.
    (Closes: #666225)

 -- gregor herrmann <gregoa@debian.org>  Sun, 03 Jun 2012 16:37:10 +0200

deja-dup (20.2-2) unstable; urgency=low

  * ACK previous NMU (Closes: #651257)
  * Delete old unused patch (Closes: #651258)
  * Drop gnome-control-center support as Gnome 3 upstream don't want 3rd
  party applications to be installed there (Closes: #650142)

 -- Jose Carlos Garcia Sogo <jsogo@debian.org>  Thu, 08 Dec 2011 12:15:32 +0100

deja-dup (20.2-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * debian/control: Drop libdbusmenu-gtk3-dev Build-dep, the package is only
    available in experimental for now (Closes: #651257)

 -- Laurent Bigonville <bigon@debian.org>  Wed, 07 Dec 2011 11:54:25 +0100

deja-dup (20.2-1) unstable; urgency=low

  * New upstream release (Closes: #613336, #632568)
     - Fixed GPG error during restore (Closes: #624598)
     - Transitioned to use nautilus 3 (Closes: #637312)
  * Remove .la files (Closes: #621293)
  * debian/rules:
    - Convert to dh8
  * debian/source/format:
    - Convert to 3.0 (quilt)
  * debian/control:
    - Update description
    - Needs libnotify >= 0.7.0
  * debian/copyright:
    - Update from upstream AUTHORS
    - Standards-Version: bump to 3.9.2, no changes needed.
    - Change yelp Build-Dep to just itstool

  (These changes where taken from Ubuntu package, thanks to Michael Terry)

 -- Jose Carlos Garcia Sogo <jsogo@debian.org>  Fri, 25 Nov 2011 21:11:10 +0100

deja-dup (14.2-1) unstable; urgency=low

  * New upstream release
    + Fixes problem to external disks (Closes: #583218)
  * Bump dependency in duplicity to 0.6.07 (Closes: #546520)

 -- Jose Carlos Garcia Sogo <jsogo@debian.org>  Thu, 08 Jul 2010 16:46:39 +0200

deja-dup (14.0-1) unstable; urgency=low

  * New upstream version (Closes: #574938)

 -- Jose Carlos Garcia Sogo <jsogo@debian.org>  Thu, 01 Apr 2010 11:55:08 +0200

deja-dup (13.6-1) unstable; urgency=low

  * New upstream release (Closes: #567124)
    + Adds menu entry to System category (Closes: #544455)
  * Depends on gvfs-fuse (Closes: #546506)
  * Bump Standards-Version to 3.8.4. No changes needed.

 -- Jose Carlos Garcia Sogo <jsogo@debian.org>  Thu, 28 Jan 2010 00:13:03 +0100

deja-dup (10.1-2) unstable; urgency=low

  * intltool is a build-dependency (Closes: #544451)
  * Bump Standards-Version to 3.8.1. No changes needed.
  * Add to debian/copyright file every (C) bit in sources.
    Thanks to Torsten Werner.

 -- Jose Carlos Garcia Sogo <jsogo@debian.org>  Mon, 31 Aug 2009 23:16:58 +0200

deja-dup (10.1-1) unstable; urgency=low

  * New Upstream Version
  * Docs have been relicensed to GPLv3.

 -- Jose Carlos Garcia Sogo <jsogo@debian.org>  Tue, 04 Aug 2009 23:34:17 +0200

deja-dup (9.1-1) unstable; urgency=low

  * New Upstream Version

 -- Jose Carlos Garcia Sogo <jsogo@debian.org>  Tue, 28 Apr 2009 23:47:27 +0200

deja-dup (8.1-1) unstable; urgency=low

  * Inital Debian release, based on Ubuntu packages (Closes: #506590)
  * debian/control:
    - Change maintainer to me.
    - Bump Standards-Version to 3.8.1. No changes needed.
    - Move -dbg package to 'debug' section.

 -- Jose Carlos Garcia Sogo <jsogo@debian.org>  Tue, 14 Apr 2009 00:19:30 +0200

deja-dup (8.1-0intrepid1) intrepid; urgency=low

  * New upstream release

 -- Michael Terry <mike@mterry.name>  Fri, 03 Apr 2009 17:59:44 -0400

deja-dup (8.1-0hardy1) hardy; urgency=low

  * New upstream release

 -- Michael Terry <mike@mterry.name>  Fri, 03 Apr 2009 17:58:44 -0400

deja-dup (8.0-0intrepid1) intrepid; urgency=low

  * New upstream release

 -- Michael Terry <mike@mterry.name>  Sun, 29 Mar 2009 11:53:45 -0400

deja-dup (8.0-0hardy1) hardy; urgency=low

  * Initial hardy release

 -- Michael Terry <mike@mterry.name>  Sun, 29 Mar 2009 11:43:28 -0400

deja-dup (7.4-0ubuntu2) jaunty; urgency=low

  * debian/control:
    - Use ${misc:Depends} for -dbg package
    - Use unique summary for -dbg package
    - Indent bullet lists an extra time
  * debian/rules:
    - Exclude nautilus plugin from makeshlibs
  * debian/patches/02_check_notify_caps.patch:
    - Patch from upstream to work with notify-osd. LP: #334630

 -- Michael Terry <mike@mterry.name>  Wed, 25 Feb 2009 20:53:07 -0500

deja-dup (7.4-0ubuntu1) jaunty; urgency=low

  * Initial release (LP: #324388)

 -- Michael Terry <mike@mterry.name>  Thu, 12 Feb 2009 07:18:01 -0500

deja-dup (7.4-0~intrepid1) intrepid; urgency=low

  * New upstream release

 -- Michael Terry <mike@mterry.name>  Wed, 11 Feb 2009 21:17:47 -0500

deja-dup (7.3-0~intrepid1) intrepid; urgency=low

  * New upstream release

 -- Michael Terry <mike@mterry.name>  Tue, 03 Feb 2009 20:40:01 -0500

deja-dup (7.2-0~intrepid1) intrepid; urgency=low

  * New upstream release

 -- Michael Terry <mike@mterry.name>  Mon, 02 Feb 2009 20:49:16 -0500

deja-dup (7.1-0~intrepid1) intrepid; urgency=low

  * New upstream release

 -- Michael Terry <mike@mterry.name>  Thu, 29 Jan 2009 15:54:19 -0500

deja-dup (7.0-0~intrepid1) intrepid; urgency=low

  * New upstream release
  * debian/control:
    - Add Build-Depend on libnautilus-extension-dev

 -- Michael Terry <mike@mterry.name>  Thu, 29 Jan 2009 08:07:33 -0500

deja-dup (6.0-0~intrepid1) intrepid; urgency=low

  * New upstream release

 -- Michael Terry <mike@mterry.name>  Mon, 19 Jan 2009 19:51:28 -0500

deja-dup (5.2-0~intrepid1) intrepid; urgency=low

  * New upstream release

 -- Michael Terry <mike@mterry.name>  Wed, 07 Jan 2009 21:47:58 -0500

deja-dup (5.1-0~intrepid1) intrepid; urgency=low

  * New upstream version

 -- Michael Terry <mike@mterry.name>  Wed, 07 Jan 2009 20:54:58 -0500

deja-dup (5.0-0~intrepid1) intrepid; urgency=low

  * New upstream version

 -- Michael Terry <mike@mterry.name>  Tue, 06 Jan 2009 20:30:09 -0500

deja-dup (4.0-0~intrepid1) intrepid; urgency=low

  * New upstream version
  * debian/control:
    - Add dbg package
    - Recommend util-linux, for ionice
    - Dropped valac from Build-Depends, not needed

 -- Michael Terry <michael.terry@canonical.com>  Mon, 29 Dec 2008 23:19:56 -0500

deja-dup (3.0~ppa1) intrepid; urgency=low

  * New upstream version
  * debian/control:
    - Add libunique-dev and libnotify-dev to Build-Depends

 -- Michael Terry <mike@mterry.name>  Thu, 04 Dec 2008 22:28:24 -0500

deja-dup (2.1-0ubuntu1~ppa1) intrepid; urgency=low

  * New upstream version

 -- Michael Terry <mike@mterry.name>  Mon, 01 Dec 2008 21:09:29 -0500

deja-dup (2.0-0ubuntu1~ppa1) intrepid; urgency=low

  * New upstream version

 -- Michael Terry <mike@mterry.name>  Sat, 22 Nov 2008 08:20:04 -0500

deja-dup (1.0-0ubuntu1~ppa1) intrepid; urgency=low

  * Initial Release.

 -- Michael Terry <michael.terry@canonical.com>  Mon, 20 Oct 2008 13:00:47 -0400
